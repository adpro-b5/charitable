package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.model.Donation;
import adpro2021.proyek.akhir.b5.campaign.repository.DonationRepo;
import adpro2021.proyek.akhir.b5.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DonationServiceImpl implements DonationService {

    // ---- Service untuk donasi yang sudah berhasil ----

    @Autowired
    private DonationRepo donationRepo;

    @Autowired
    private ReportService reportService;

    @Override
    public Donation createDonation(Donation donation) {
        donationRepo.save(donation);
        notifyReport(donation.getType(), donation.getDonationAmount());
        return donation;
    }

    @Override
    public Iterable<Donation> getListDonation() {
        return donationRepo.findAll();
    }

    @Override
    public List<Donation> getListDonationByType(String type) {
        List<Donation> listDonation = new ArrayList<>();
        for (Donation donation : donationRepo.findAll()) {
            if (donation.getType().equals(type)) {
                listDonation.add(donation);
            }
        }
        return listDonation;
    }

    @Override
    public void notifyReport(String type, int donationAmount) {
        reportService.updateReport(type, donationAmount);
    }

}