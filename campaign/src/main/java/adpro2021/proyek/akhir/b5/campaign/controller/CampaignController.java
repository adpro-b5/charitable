package adpro2021.proyek.akhir.b5.campaign.controller;

import adpro2021.proyek.akhir.b5.campaign.model.Donation;
import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;
import adpro2021.proyek.akhir.b5.campaign.service.CampaignService;
import adpro2021.proyek.akhir.b5.campaign.service.DonationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/campaign")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @Autowired
    private DonationService donationService;

    @GetMapping(path = "")
    public String allCampaign(Model model) {
        var educationalCampaign = new EducationalCampaign();
        var socialCampaign = new SocialCampaign();
        var healthCampaign = new HealthCampaign();
        model.addAttribute("eduCampaign", educationalCampaign);
        model.addAttribute("socialCampaign", socialCampaign);
        model.addAttribute("healthCampaign", healthCampaign);
        return "campaign/allCampaign";
    }

    @GetMapping(path = "/edu")
    public String eduCampaign(Model model) {
        Iterable<EducationalCampaign> educationalCampaigns = campaignService.showEduCampaign();
        model.addAttribute("type", "Educational");
        model.addAttribute("image", "edu");
        model.addAttribute("campaign", educationalCampaigns);
        // add model review khusus educationalCampaign
        return "campaign/detailCampaign";
    }

    @GetMapping(path = "/social")
    public String socialCampaign(Model model) {
        Iterable<SocialCampaign> socialCampaigns = campaignService.showSocialCampaign();
        model.addAttribute("type", "Social");
        model.addAttribute("image", "social");
        model.addAttribute("campaign", socialCampaigns);
        // add model review khusus socialCampaign
        return "campaign/detailCampaign";
    }

    @GetMapping(path = "/health")
    public String healthCampaign(Model model) {
        Iterable<HealthCampaign> healthCampaigns = campaignService.showHealthCampaign();
        model.addAttribute("type", "Health");
        model.addAttribute("image", "health");
        model.addAttribute("campaign", healthCampaigns);
        // add model review khusus healthCampaign
        return "campaign/detailCampaign";
    }

    @GetMapping(path = "/{type}/pay")
    public String paymentCampaign(@PathVariable(value = "type") String type, Model model) {
        Donation donation = new Donation();
        donation.setType(type);

        model.addAttribute("type", type);
        model.addAttribute("donation", donation);

        return "campaign/payCampaign";
    }

    @PostMapping(path = "/confirm-payment")
    public String confirmPayment(@ModelAttribute("donation") Donation donation) {
        donationService.createDonation(donation);
        return "redirect:/campaign";
    }

}
