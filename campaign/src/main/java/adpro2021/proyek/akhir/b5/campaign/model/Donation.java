package adpro2021.proyek.akhir.b5.campaign.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "donation")
@Data
public class Donation {

    // ---- Class untuk tiap donasi yang udah berhasil ----

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "idDonation", updatable = false, nullable = false)
    private int idDonation;

    @Column(name = "type")
    private String type;

    @Column(name = "bankName")
    private String bankName;

    @Column(name = "accountNumber")
    private String accountNumber;

    @Column(name = "accountName")
    private String accountName;

    @Column(name = "donationAmount")
    private int donationAmount;

    public int getIdDonation() {
        return idDonation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public int getDonationAmount() {
        return donationAmount;
    }

    public void setDonationAmount(int donationAmount) {
        this.donationAmount = donationAmount;
    }

    @Override
    public String toString() {
        return getIdDonation() + " - " +
                getType() + " - " +
                getBankName() + " - " +
                getAccountNumber() + " - " +
                getAccountName() + " - " +
                getDonationAmount();
    }

    // TESTIMONI/REVIEW : Donation - Review relasinya OneToMany
    //    @OneToMany(mappedBy = "review", fetch = FetchType.LAZY)
    //    private List<Review> Review;

    // NOTE : Mungkin di Class Review isinya ada ininya
    // @JsonBackReference
    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name = "npm")
    // private Mahasiswa mahasiswa;

}

