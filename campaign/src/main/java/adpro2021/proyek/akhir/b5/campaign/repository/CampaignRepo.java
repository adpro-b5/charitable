package adpro2021.proyek.akhir.b5.campaign.repository;

import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CampaignRepo {

    private EducationalCampaign educationalCampaign;
    private SocialCampaign socialCampaign;
    private HealthCampaign healthCampaign;

    public List<EducationalCampaign> showEduCampaign() {
        return educationalCampaign.showCampaign();
    }

    public List<SocialCampaign> showSocialCampaign() {
        return socialCampaign.showCampaign();
    }

    public List<HealthCampaign> showHealthCampaign() {
        return healthCampaign.showCampaign();
    }
}
