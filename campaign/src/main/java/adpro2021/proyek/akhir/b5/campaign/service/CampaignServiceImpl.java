package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {

    private final EducationalCampaign educationalCampaign;
    private final SocialCampaign socialCampaign;
    private final HealthCampaign healthCampaign;

    public CampaignServiceImpl() {
        educationalCampaign = new EducationalCampaign();
        socialCampaign = new SocialCampaign();
        healthCampaign = new HealthCampaign();
    }

    @Override
    public List showEduCampaign() {
        return educationalCampaign.showCampaign();
    }

    @Override
    public List showSocialCampaign() {
        return socialCampaign.showCampaign();
    }

    @Override
    public List showHealthCampaign() {
        return healthCampaign.showCampaign();
    }
}
