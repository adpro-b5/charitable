package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.model.Donation;

import java.util.List;

public interface DonationService {

    // ---- Interface service untuk donasi yang sudah berhasil ----

    Donation createDonation(Donation donation);
    Iterable<Donation> getListDonation();
    List<Donation> getListDonationByType(String type);
    void notifyReport(String type, int donationAmount);

    // NOTE : Review dihandle di class Reviewnya,
    //          ada ini di ReviewService
    //
    //          @Autowired
    //          private DonationRepo donationRepo;
    //          @Autowired
    //          private ReviewRepository reviewRepository;
    //
    //          @Override
    //          public Review createReview(Review review, int idDonation) {
    //              Donation donation = donationRepo.findById(idDonation);
    //              review.setDonation(donasi);
    //              reviewRepository.save(review);
    //              return review;
    //          }
}

