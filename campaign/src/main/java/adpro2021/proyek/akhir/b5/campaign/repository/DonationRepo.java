package adpro2021.proyek.akhir.b5.campaign.repository;

import adpro2021.proyek.akhir.b5.campaign.model.Donation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DonationRepo extends JpaRepository<Donation, Integer> {
    //    ---- Repo untuk donasi yang udah berhasil ----
    Donation findByIdDonation(int idDonation);

}
