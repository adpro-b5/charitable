package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;

public interface CampaignService {

    Iterable<EducationalCampaign> showEduCampaign();
    Iterable<SocialCampaign> showSocialCampaign();
    Iterable<HealthCampaign> showHealthCampaign();

}
