package adpro2021.proyek.akhir.b5.campaign.core;

public class SocialCampaign extends Campaign{

    @Override
    public String getOverview() {
        return "People's lives, destiny, luck, are not the same. " +
                "Poverty and disasters are keep happening. " +
                "Humanity is calling you here now.";
    }

    @Override
    public String getDescription() {
        return "Many people still suffer from poverty. " +
                "They have to work all day and night to make a living " +
                "or even just to eat, not even think of school and the future. " +
                "On the other hand, we also see that disasters keep happening. " +
                "People suddenly lose their precious things, their home, " +
                "or even left with nothing. Humanity is calling you right " +
                "now to help them from their suffering. Your donation will " +
                "be sent to those who needs it.";
    }

}
