package adpro2021.proyek.akhir.b5.campaign.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DonationTest {

    private Donation donation;

    @BeforeEach
    public void setUp()throws Exception {
        this.donation = new Donation();
    }

    @Test
    void testSetBankNameMethod() throws Exception {
        donation.setBankName("namabank");
        String hasil = donation.getBankName();
        Assertions.assertEquals("namabank", hasil);
    }

    @Test
    void testSetAccountNumberMethod() throws Exception {
        donation.setAccountNumber("1234567890");
        String hasil = donation.getAccountNumber();
        Assertions.assertEquals("1234567890", hasil);
    }

    @Test
    void testSetAccountNameMethod() throws Exception {
        donation.setAccountName("namaakun");
        String hasil = donation.getAccountName();
        Assertions.assertEquals("namaakun", hasil);
    }

    @Test
    void testSetDonationAmountMethod() throws Exception {
        donation.setDonationAmount(50000);
        int hasil = donation.getDonationAmount();
        Assertions.assertEquals(50000, hasil);
    }
}
