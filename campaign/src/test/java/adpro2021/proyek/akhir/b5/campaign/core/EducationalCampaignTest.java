package adpro2021.proyek.akhir.b5.campaign.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class EducationalCampaignTest {

    private Class<?> educationalCampaignClass;

    @BeforeEach
    public void setUp() throws Exception {
        educationalCampaignClass = Class.forName("adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign");
    }

    @Test
    public void testEducationalCampaignIsConcreteClass() {
        assertFalse(Modifier.isAbstract(educationalCampaignClass.getModifiers()));
    }

    @Test
    public void testEducationalCampaignIsACampaign() {
        Class<?> parentClass = educationalCampaignClass.getSuperclass();
        assertEquals("adpro2021.proyek.akhir.b5.campaign.core.Campaign", parentClass.getName());
    }

    @Test
    public void testOverrideGetOverview() throws Exception {
        Method getOverview = educationalCampaignClass.getDeclaredMethod("getOverview");
        assertEquals("java.lang.String", getOverview.getGenericReturnType().getTypeName());
        assertEquals(0, getOverview.getParameterCount());
    }

    @Test
    public void testOverrideGetDescription() throws Exception {
        Method getDescription = educationalCampaignClass.getDeclaredMethod("getDescription");
        assertEquals("java.lang.String", getDescription.getGenericReturnType().getTypeName());
        assertEquals(0, getDescription.getParameterCount());
    }

    @Test
    public void testReturnOverview() {
        EducationalCampaign educationalCampaign = new EducationalCampaign();
        String hasil = "Everyone has the right to pursue their dreams, " +
                "but some of them can’t even go to school due to " +
                "lack of financial support.";
        assertEquals(hasil, educationalCampaign.getOverview());
    }
}

