package adpro2021.proyek.akhir.b5.campaign.repository;

import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.stereotype.Repository;
import org.springframework.test.util.ReflectionTestUtils;

import static org.assertj.core.api.Assertions.assertThat;

@Repository
public class CampaignRepoTest {

    private CampaignRepo campaignRepo;

    @Mock
    private EducationalCampaign educationalCampaign;

    @Mock
    private SocialCampaign socialCampaign;

    @Mock
    private HealthCampaign healthCampaign;

    @BeforeEach
    public void setUp() {
        campaignRepo = new CampaignRepo();
        educationalCampaign = new EducationalCampaign();
        socialCampaign = new SocialCampaign();
        healthCampaign = new HealthCampaign();
    }

    @Test
    public void whenCampaignRepoShowEduCampaignShouldReturnEduCampaign() {
        ReflectionTestUtils.setField(campaignRepo, "educationalCampaign", new EducationalCampaign());
        Iterable<EducationalCampaign> calledEduCampaign = campaignRepo.showEduCampaign();

        assertThat(calledEduCampaign).isEqualTo(educationalCampaign.showCampaign());
    }

    @Test
    public void whenCampaignRepoShowSocialCampaignShouldReturnSocialCampaign() {
        ReflectionTestUtils.setField(campaignRepo, "socialCampaign", new SocialCampaign());
        Iterable<SocialCampaign> calledSocialCampaign = campaignRepo.showSocialCampaign();

        assertThat(calledSocialCampaign).isEqualTo(socialCampaign.showCampaign());
    }

    @Test
    public void whenCampaignRepoShowHealthCampaignShouldReturnHealthCampaign() {
        ReflectionTestUtils.setField(campaignRepo, "healthCampaign", new HealthCampaign());
        Iterable<HealthCampaign> calledHealthCampaign = campaignRepo.showHealthCampaign();

        assertThat(calledHealthCampaign).isEqualTo(healthCampaign.showCampaign());
    }

}
