package adpro2021.proyek.akhir.b5.campaign.controller;

import adpro2021.proyek.akhir.b5.campaign.service.CampaignService;
import adpro2021.proyek.akhir.b5.campaign.service.DonationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = CampaignController.class)
class CampaignControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CampaignService campaignService;

    @MockBean
    private DonationService donationService;

    @Test
    public void whenCampaignURLIsAccesedShouldContainCampaignModel() throws Exception {
        mockMvc.perform(get("/campaign"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("eduCampaign"))
                .andExpect(view().name("campaign/allCampaign"));
    }

    @Test
    public void whenDetailEduCampaignURLIsAccesedShouldContainCampaignModel() throws Exception {
        mockMvc.perform(get("/campaign/edu"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("campaign"))
                .andExpect(model().attributeExists("type"))
                .andExpect(model().attributeExists("image"))
                .andExpect(view().name("campaign/detailCampaign"));
    }

    @Test
    public void whenDetailSocialCampaignURLIsAccesedShouldContainCampaignModel() throws Exception {
        mockMvc.perform(get("/campaign/social"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("campaign"))
                .andExpect(model().attributeExists("type"))
                .andExpect(model().attributeExists("image"))
                .andExpect(view().name("campaign/detailCampaign"));
    }

    @Test
    public void whenDetailHealthCampaignURLIsAccesedShouldContainCampaignModel() throws Exception {
        mockMvc.perform(get("/campaign/health"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("campaign"))
                .andExpect(model().attributeExists("type"))
                .andExpect(model().attributeExists("image"))
                .andExpect(view().name("campaign/detailCampaign"));
    }

    @Test
    public void whenPaymentURLIsAccesedShouldContainDonationModel() throws Exception {
        mockMvc.perform(get("/campaign/edu/pay"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("type"))
                .andExpect(model().attributeExists("donation"))
                .andExpect(view().name("campaign/payCampaign"));
    }

    @Test
    public void whenConfirmPaymentURLIsAccessedShouldCreateDonation() throws Exception {
        mockMvc.perform(post("/campaign/confirm-payment"))
                .andExpect(handler().methodName("confirmPayment"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/campaign"));

    }
}

