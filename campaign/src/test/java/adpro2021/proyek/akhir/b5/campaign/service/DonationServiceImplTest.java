package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.model.Donation;
import adpro2021.proyek.akhir.b5.campaign.repository.DonationRepo;
import adpro2021.proyek.akhir.b5.report.service.ReportService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class DonationServiceImplTest {

    @Mock
    private DonationRepo donationRepo;

    @InjectMocks
    private DonationServiceImpl donationService;

    @Mock
    private ReportService reportService;

    private static Donation donation = new Donation();

    @BeforeAll
    static void setUp() {
        donation.setType("Educational");
        donation.setBankName("bank");
        donation.setAccountName("abc");
        donation.setAccountNumber("123");
        donation.setDonationAmount(500);
    }

    @Test
    void testCreateDonation() {
        lenient().when(donationRepo.save(any(Donation.class))).then(returnsFirstArg());
        Donation newDonation = donationService.createDonation(donation);
        assertEquals(donation, newDonation);
    }

    @Test
    void testGetListDonation() {
        List<Donation> expectedList = Collections.singletonList(donation);
        lenient().when(donationRepo.findAll()).thenReturn(expectedList);
        Iterable<Donation> actualList = donationService.getListDonation();
        assertEquals(expectedList, actualList);
    }

    @Test
    void testGetListDonationByType() {
        List<Donation> expectedList = Collections.singletonList(donation);
        lenient().when(donationRepo.findAll()).thenReturn(expectedList);
        Iterable<Donation> actualList = donationService.getListDonationByType("Educational");
        assertEquals(expectedList, actualList);
    }
}
