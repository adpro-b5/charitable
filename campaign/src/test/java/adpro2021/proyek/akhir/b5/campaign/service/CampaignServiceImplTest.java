package adpro2021.proyek.akhir.b5.campaign.service;

import adpro2021.proyek.akhir.b5.campaign.core.EducationalCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.HealthCampaign;
import adpro2021.proyek.akhir.b5.campaign.core.SocialCampaign;
import adpro2021.proyek.akhir.b5.campaign.repository.CampaignRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CampaignServiceImplTest {

    @Mock
    private CampaignRepo campaignRepo;

    @InjectMocks
    private CampaignServiceImpl campaignService;

    @Test
    public void whenShowEduCampaignShouldEduShowCampaign() {
        EducationalCampaign educationalCampaign = new EducationalCampaign();
        Iterable<EducationalCampaign> calledEduCampaign = campaignService.showEduCampaign();
        assertThat(calledEduCampaign).isEqualTo(educationalCampaign.showCampaign());
    }

    @Test
    public void whenShowSocialCampaignShouldSocialShowCampaign() {
        SocialCampaign socialCampaign = new SocialCampaign();
        Iterable<SocialCampaign> calledSocialCampaign = campaignService.showSocialCampaign();
        assertThat(calledSocialCampaign).isEqualTo(socialCampaign.showCampaign());
    }

    @Test
    public void whenShowHealthCampaignShouldHealthShowCampaign() {
        HealthCampaign healthCampaign = new HealthCampaign();
        Iterable<HealthCampaign> calledHealthCampaign = campaignService.showHealthCampaign();
        assertThat(calledHealthCampaign).isEqualTo(healthCampaign.showCampaign());
    }
}
