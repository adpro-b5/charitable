package adpro2021.proyek.akhir.b5.charitable.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CharitAbleController {

    @GetMapping(path = "")
    public String getHome(){
        return "home";
    }
}
