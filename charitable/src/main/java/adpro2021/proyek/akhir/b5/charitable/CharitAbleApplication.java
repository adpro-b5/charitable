package adpro2021.proyek.akhir.b5.charitable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "adpro2021.proyek.akhir.b5")
@EntityScan(basePackages = {"adpro2021.proyek.akhir.b5"} )
@EnableJpaRepositories(basePackages = {"adpro2021.proyek.akhir.b5"})
public class CharitAbleApplication {
    public static void main(String[] args) {
        SpringApplication.run(CharitAbleApplication.class, args);
    }
}