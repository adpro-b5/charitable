package adpro2021.proyek.akhir.b5.report.service;

import adpro2021.proyek.akhir.b5.report.core.Report;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class ReportServiceImplTest {

    private static ReportServiceImpl reportServiceImpl;

    @BeforeAll
    public static void setUp() {
        reportServiceImpl = new ReportServiceImpl();
    }

    @Test
    void whenUpdateReportMethodIsCalled() {
        reportServiceImpl.updateReport("Educational", 100);

        List<Report> listOfReport = reportServiceImpl.getReport();
        for (Report report : listOfReport) {
            if (report.getName().equals("Educational")) {
                assertEquals(100, report.getTotalDonation());
            }
        }
    }
}
