package adpro2021.proyek.akhir.b5.report.core;

public class EducationalReport extends Report  {

    public EducationalReport(ReportObserver reportObserver) {
        this.name = "Educational Campaign";
        this.reportObserver = reportObserver;
        this.image = "/campaign/edu.png";
    }

    @Override
    public void update() {
        String donationType = reportObserver.getType();

        if (donationType.equals("Educational")) {
            addTotalDonation(reportObserver.getDonationAmount());
        }
    }
}
