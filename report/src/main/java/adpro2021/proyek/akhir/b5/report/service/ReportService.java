package adpro2021.proyek.akhir.b5.report.service;

import adpro2021.proyek.akhir.b5.report.core.Report;

import java.util.List;

public interface ReportService {

    List<Report> getReport();
    void updateReport(String type, int donationAmount);
}
