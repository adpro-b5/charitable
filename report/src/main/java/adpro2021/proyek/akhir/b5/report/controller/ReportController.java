package adpro2021.proyek.akhir.b5.report.controller;

import adpro2021.proyek.akhir.b5.report.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/report")
    public String getReport(Model model){
        model.addAttribute("reports", reportService.getReport());
        return "allReports";
    }
}
