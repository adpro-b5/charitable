package adpro2021.proyek.akhir.b5.report.core;

public class HealthReport extends Report {

    public HealthReport(ReportObserver reportObserver) {
        this.name = "Health Campaign";
        this.reportObserver = reportObserver;
        this.image = "/campaign/health.png";
    }

    @Override
    public void update() {
        String donationType = reportObserver.getType();

        if (donationType.equals("Health")) {
            addTotalDonation(reportObserver.getDonationAmount());
        }
    }

}
