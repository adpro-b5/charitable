package adpro2021.proyek.akhir.b5.report.service;

import adpro2021.proyek.akhir.b5.report.core.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private final ReportObserver reportObserver;

    public ReportServiceImpl() {
        reportObserver = new ReportObserver();

        var educationalReport = new EducationalReport(reportObserver);
        var socialReport = new SocialReport(reportObserver);
        var healthReport = new HealthReport(reportObserver);

        reportObserver.add(educationalReport);
        reportObserver.add(socialReport);
        reportObserver.add(healthReport);
    }

    @Override
    public List<Report> getReport() {
        return reportObserver.getListOfReports();
    }

    @Override
    public void updateReport(String type, int donationAmount) {
        reportObserver.addDonation(type, donationAmount);
    }

}
