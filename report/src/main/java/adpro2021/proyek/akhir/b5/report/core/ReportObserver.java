package adpro2021.proyek.akhir.b5.report.core;

import java.util.ArrayList;
import java.util.List;

public class ReportObserver {

    private static List<Report> listOfReports = new ArrayList<>();
    private String type;
    private int donationAmount;

    public void add(Report report) {
        listOfReports.add(report);
    }

    public void addDonation(String type, int donationAmount) {
        this.type = type;
        this.donationAmount = donationAmount;
        broadcast();
    }

    public List<Report> getListOfReports() {
        return listOfReports;
    }

    public void broadcast() {
        for(Report report : listOfReports) {
            report.update();
        }
    }

    public String getType() {
        return type;
    }

    public int getDonationAmount() {
        return donationAmount;
    }
}
