#### Campaign
[![pipeline status](https://gitlab.com/adpro-b5/charitable/badges/campaign-adian/pipeline.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/campaign-adian)
[![coverage report](https://gitlab.com/adpro-b5/charitable/badges/campaign-adian/coverage.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/campaign-adian)

#### Report
[![pipeline status](https://gitlab.com/adpro-b5/charitable/badges/report/pipeline.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/report)
[![coverage report](https://gitlab.com/adpro-b5/charitable/badges/report/coverage.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/report)

#### Article
[![pipeline status](https://gitlab.com/adpro-b5/charitable/badges/article/pipeline.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/article)
[![coverage report](https://gitlab.com/adpro-b5/charitable/badges/article/coverage.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/article)

#### Review
[![pipeline status](https://gitlab.com/adpro-b5/charitable/badges/review/pipeline.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/review)
[![coverage report](https://gitlab.com/adpro-b5/charitable/badges/review/coverage.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/review)

#### User
[![pipeline status](https://gitlab.com/adpro-b5/charitable/badges/user/pipeline.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/user)
[![coverage report](https://gitlab.com/adpro-b5/charitable/badges/user/coverage.svg)](https://gitlab.com/adpro-b5/charitable/-/commits/user)

---

# CharitAble

#### Kelompok B5

* **Amanda Carrisa Ashardian (1906399966)** - Article
* **Annisa Dian Nugrahani (1906292906)** - Campaign
* **Fairuza Raryasdya Ayunda (1906285610)** - Pembayaran & Laporan Donasi
* **Fandy David Rivaldy (1506757970)** - Register, Login, dan Profile
* **Muhammad Ali Asadillah (1806186660)** - Review
